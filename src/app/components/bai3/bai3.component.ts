import {Component} from '@angular/core';
import {RouterLink, RouterLinkActive} from "@angular/router";


@Component({
  selector: 'app-bai3',
  standalone: true,
  imports: [
    RouterLink,
    RouterLinkActive
  ],
  templateUrl: './bai3.component.html',
  styleUrl: './bai3.component.css'
})
export class Bai3Component {
  inventors: Inventor[] = [
    {
      id: 1,
      first: 'Thomas',
      last: 'Edison',
      year: 1847,
      passed: 1931,
    },
    {
      id: 2,
      first: 'Nikola',
      last: 'Tesla',
      year: 1856,
      passed: 1943,
    },
    {
      id: 3,
      first: 'Albert',
      last: 'Einstein',
      year: 1879,
      passed: 1955,
    },
    {
      id: 4,
      first: 'Marie',
      last: 'Curie',
      year: 1867,
      passed: 1934,
    },
    {
      id: 5,
      first: 'Isaac',
      last: 'Newton',
      year: 1643,
      passed: 1727,
    },
  ];
}
