import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainLabMenuComponent } from './main-lab-menu.component';

describe('MainLabMenuComponent', () => {
  let component: MainLabMenuComponent;
  let fixture: ComponentFixture<MainLabMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MainLabMenuComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MainLabMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
