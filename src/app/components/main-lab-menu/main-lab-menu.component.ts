import { Component } from '@angular/core';
import {RouterLink, RouterLinkActive} from "@angular/router";

@Component({
  selector: 'app-main-lab-menu',
  standalone: true,
  imports: [
    RouterLink,
    RouterLinkActive
  ],
  templateUrl: './main-lab-menu.component.html',
  styleUrl: './main-lab-menu.component.css'
})
export class MainLabMenuComponent {

}
