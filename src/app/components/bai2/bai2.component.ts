import { Component } from '@angular/core';
import {RouterLink, RouterLinkActive} from "@angular/router";

@Component({
  selector: 'app-bai2-1',
  standalone: true,
  imports: [
    RouterLink,
    RouterLinkActive
  ],
  templateUrl: './bai2.component.html',
  styleUrl: './bai2.component.css'
})
export class Bai2Component {
  title = 'FPT POLYTECHNIC'
  student = {
    name: "Nguyễn Đinh Tiến Điền",
    gender: "Nam",
    birthDate: "09/07/2004",
    image: 'myBeautifullGirlImage.jpg',
    averagePoint : 9.2
  }
}
