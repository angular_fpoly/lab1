import {Routes} from '@angular/router';
import {Bai2Component} from './components/bai2/bai2.component';
import { Bai3Component } from "./components/bai3/bai3.component";
import {MainLabMenuComponent} from "./components/main-lab-menu/main-lab-menu.component";
import {Bai4Component} from "./components/bai4/bai4.component";

export const routes: Routes = [
  {"path": "", component: MainLabMenuComponent},
  {"path": "bai2", component: Bai2Component},
  {"path": "bai3", component: Bai3Component},
  {"path": "bai4", component: Bai4Component},
];
